<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m171127_204741_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'cost' => $this->float()->defaultValue(0),
            'start_at' => $this->dateTime()->notNull(),
            'end_at' => $this->dateTime()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        echo "m171127_204741_create_project_table migration complete.\n";
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
        echo "m171127_204741_create_project_table reverted successfully.\n";
    }
}
