<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171127_184702_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->unsigned()->defaultValue(null),
            'username' => $this->string()->notNull(),
            'login' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('user', [
            'username' => 'admin',
            'login' => 'admin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'created_at' => mktime(),
        ]);
        echo "m171127_184702_create_user_table migration complete.\n";
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete('user', ['id' => 1]);
        $this->dropTable('user');
        echo "m171127_184702_create_user_table reverted successfully.\n";
    }
}
