<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Project;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            /*'user_id',*/
            [
                'attribute'=>'user_id',
                'format'=>'text',
                'label' => 'Assigned User',
                'content'=>function($data){
                    return $data->getUserName();
                },
            ],
            'name',
            'cost',
            'start_at',
            'end_at',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view_project} {update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to([$action,'id'=>$key]);
                },
                'buttons' => [
                    'view_project' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['/api/project', 'id' => $model->id],
                            [
                                'title' => 'View',
                                'data-pjax' => '0',
                                'href' => $url,
                            ]
                        );
                    }
                ],
            ],
        ],
    ]); ?>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>