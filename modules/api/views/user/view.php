<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Show User \''. $model->username.'\'';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => Url::to("api/users", true)];
$this->params['breadcrumbs'][] = $model->username;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            /*'project_id',*/
            [
                'format'=>'html',
                'label' => 'User Projects',
                'value' => $model->getHtmlProjectsList($model->getId()),
                /*'contentOptions' => ['class' => 'bg-red'],     // настройка HTML атрибутов для тега, соответсвующего value
                'captionOptions' => ['tooltip' => 'Tooltip'],  // настройка HTML атрибутов для тега, соответсвующего label*/
            ],
            'username',
            'login',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', Url::to("api/users/$model->id/update", true), ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', Url::to("api/users/$model->id/delete", true), [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancel', Url::to("api/users", true), ['class' => 'btn btn-cancel']) ?>
    </p>
</div>
