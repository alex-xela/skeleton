<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            /*'project_id',*/
            [
                'attribute'=>'project_id',
                'format'=>'text',
                'label' => 'Total Projects',
                'content'=>function($data){
                    //var_dump($data);
                    return $data->getProjectsCount($data);
                },
            ],
            'username',
            'login',
            'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'status',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view_user} {update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to([$action,'id'=>$key]);
                },
                'buttons' => [
                    'view_user' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['/api/user', 'id' => $model->id],
                            [
                                'title' => 'View',
                                'data-pjax' => '0',
                                'href' => $url,
                            ]
                        );
                    },
                    /*'generate' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-link"></span>',
                            ['users/link', 'id' => $model->id],
                            [
                                'title' => 'Generate Reset Link',
                                'data-pjax' => '0',
                                'href' => $url,
                            ]
                        );
                    },*/
                ],
            ],
        ],
    ]);

    ?>

    <p>
        <?= Html::a('Create User', Url::to('/api/users/create', true), ['class' => 'btn btn-success']) ?>
    </p>

</div>
