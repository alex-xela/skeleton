<?php

use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $params = [
        'prompt' => 'Select Assigned User ...'
    ];
//    $form->field($model, 'user_id')->dropDownList([0=>'0', 1=>'1'], $params);
    ?>

    <div class="form-group field-project-name required">
        <label class="control-label" for="project-user_id">Assigned User</label>
        <?= Html::activeDropDownList($model, 'user_id',ArrayHelper::map(User::find()->all(),'id','username'),['prompt'=>'Select User', 'class' => 'form-control']) ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'start_at')->textInput() ?>

    <?= $form->field($model, 'end_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>