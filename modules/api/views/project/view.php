<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = 'Show Project \''.$model->name.'\'';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => Url::to('/api/projects')];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            /*'user_id',*/
            [
                'label'=>'Assigned User',
                'format'=>'text', // Возможные варианты: raw, html
                'value' => $model->getUserName($model->getId()),
            ],
            'name',
            'cost',
            'start_at',
            'end_at',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', Url::to("api/projects/$model->id/update", true), ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', Url::to("api/projects/$model->id/delete", true), [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancel', Url::to("api/projects", true), ['class' => 'btn btn-cancel']) ?>
    </p>

</div>