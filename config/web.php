<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'RBYSkotF1vz07baZZIcy0nvEW8rUDWsJ',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [

               /* 'api/<controller:\w+>/<action:\w+>'                    => 'api/<controller>/<action>',
                'api/<controller:\w+>/<action:update|delete>/<id:\d+>'  => 'api/<controller>/<action>',*/

                ''=>'site/index',
                '<action:(index|login|logout)>'                     => 'site/<action>',
                '<controller:\w+>/<id:\d+>'                         => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'            => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'                     => '<controller>/<action>',

                /*'api'=>'site/api',
                '<action:(index|view|create|update|delete)>'=>'site/api/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view/api',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',*/

                /*'<controller:(user|project)>/<id:\d+>/<action:(create|update|delete)>' => '<controller>/<action>',
                '<controller:(user|project)>/<id:\d+>' => '<controller>/view',
                '<controller:(user|project)>s' => '<controller>/index',*/

                // REST routes for CRUD operations
//                'api/<controller:\w+>s'                                 => 'api/<controller>/index',
                'api/<controller:\w+>/<id:\d+>'                         => 'api/<controller>/view',
                'POST api/<controller:\w+>'                             => 'api/<controller>/index', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
                'PUT api/<controller:\w+>/<id:\d+>'                     => 'api/<controller>/update', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
                'DELETE api/<controller:\w+>/<id:\d+>'                  => 'api/<controller>/delete', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	            // normal routes for CRUD operations
	            'api/<controller:\w+>s/create'                          => 'api/<controller>/create',
	            'api/<controller:\w+>/<id:\d+>/<action:update|delete|link>'
                                                                        => 'api/<controller>/<action>',
            ],
        ],

        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/adminLTE'],
                'baseUrl' => '@web/../themes/adminLTE',
            ],
        ],
    ],
    'params' => $params,
    'layout'=>'column2',
    'layoutPath'=>'@app/themes/adminLTE/layouts',
];
$config['modules']['api'] = [
    'class' => 'app\modules\api\User',
];
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [ //here
            'crud' => [ // generator name
                'class' => 'yii\gii\generators\crud\Generator', // generator class
                'templates' => [ //setting for out templates
                    'custom' => '@app/vendor/yiisoft/yii2-gii/generators/crud/custom', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
