<?php

namespace app\models;

use Faker\Provider\DateTime;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Project model
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $cost
 * @property DateTime $start_at
 * @property DateTime $end_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class Project extends ActiveRecord
{
    public static function getUsersList()
    {
        $users = User::find()
            ->select(['user.id', 'user.username'])
            ->join('JOIN', 'project p', 'p.user_id = user.id')
            ->distinct(true)
            ->all();
        return ArrayHelper::map($users, 'id', 'username');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName()
    {
        $user = $this->user;
        return $user ? $user->username : '';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'cost', 'start_at', 'end_at'], 'required'],
            [['name'], 'string'],
            [['cost'], 'number'],
            [['start_at', 'end_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Finds user by project name
     *
     * @param string $name
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::findOne(['name' => $name]);
    }
}
